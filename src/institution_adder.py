import csv
from institutions.models import *

with open("schools_details.csv") as f:
	data = csv.reader(f , delimiter = ",")
	data =  list(data)
	
data = data[1:]

k = 0 

for i in data:
	if(k % 100 == 0): print k 
	k = k + 1 
	school_code = i[1]
	school_name = i[2]
	school_level = i[3]
	Dist = None
	for d in District.objects.all().filter(name = i[4]):
		Dist = d
	if(Dist == None): Dist = District(name = i[4] ) ; Dist.save()

	eduDist = None
	for d in EduDistrict.objects.all().filter(name = i[5]):
		eduDist = d
	if(eduDist == None): eduDist = EduDistrict(name = i[5] , district = Dist ) ; eduDist.save()

	subDist = None 
	for d in SubDistrict.objects.all().filter(name = i[6]):
		subDist = d
	if(subDist == None): subDist = SubDistrict(name = i[6] , edu_district = eduDist) ; subDist.save()

	school_adress = i[7]
	school_financial = i[8]


	Insititution(code = school_code , name = school_name , level = school_level , sub_district = subDist , address = school_adress , finance_type = school_financial).save()

	
