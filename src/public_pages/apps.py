from __future__ import unicode_literals

from django.apps import AppConfig


class PublicPagesConfig(AppConfig):
    name = 'public_pages'
