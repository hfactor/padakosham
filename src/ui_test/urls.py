from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^unlogin', views.login, name='unlogin'),
    url(r'^unsignup', views.signup, name='unsignup'),
    url(r'^unadd_student', views.add_student, name='unadd_student'),
    url(r'^unhome', views.home, name='home'),
]
