from django.contrib import admin
from django import forms
from .models import *
# Register your models here.

class InstitutionAdminForm(forms.ModelForm):
    class Meta:
        model = Institution
        fields = '__all__'


class InstitutionModelAdmin(admin.ModelAdmin):
    form = InstitutionAdminForm
    list_display = ['code', 'name', 'level', 'sub_district','address', 'finance_type']
    list_editable = ['name', 'level', 'sub_district','address', 'finance_type']
    
    class Meta:
        model = Institution
        fields = '__all__'

admin.site.register(Institution, InstitutionModelAdmin)



class SubDistrictAdminForm(forms.ModelForm):
    class Meta:
        model = SubDistrict
        fields = '__all__'


class SubDistrictModelAdmin(admin.ModelAdmin):
    form = SubDistrictAdminForm
    list_display = ['name', 'edu_district']
    list_editable = ['edu_district']    
    class Meta:
        model = SubDistrict
        fields = '__all__'

admin.site.register(SubDistrict, SubDistrictModelAdmin)




class EduDistrictAdminForm(forms.ModelForm):
    class Meta:
        model = EduDistrict
        fields = '__all__'


class EduDistrictModelAdmin(admin.ModelAdmin):
    form = EduDistrictAdminForm
    list_display = ['district']
        
    class Meta:
        model = EduDistrict
        fields = '__all__'

admin.site.register(EduDistrict,EduDistrictModelAdmin)



class DistrictAdminForm(forms.ModelForm):
    class Meta:
        model = District
        fields = '__all__'


class DistrictModelAdmin(admin.ModelAdmin):
    form = DistrictAdminForm
    list_display = ['name']
    
    
    class Meta:
        model = District
        fields = '__all__'

admin.site.register(District, DistrictModelAdmin)

