from django.contrib import admin
from .models import *
# Register your models here.
class RPProfileModelAdmin(admin.ModelAdmin):
    list_display = ['user','institution']

    class Meta:
        model = RPProfile


admin.site.register(RPProfile, RPProfileModelAdmin)

