from django import forms
from cuser.forms import UserCreationForm
from django.forms import ModelForm
from django.conf import settings
from institutions.models import Institution
User = settings.AUTH_USER_MODEL

class UserForm(UserCreationForm):
    password_field_kwargs = {'strip': False}
    email = forms.EmailField(
                label=(""),
                max_length=254,
                widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            )
    password1 = forms.CharField(
                    label=(""),
                    widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control'}),
                    **password_field_kwargs
                )
    password2 = forms.CharField(
                    label=(""),
                    widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password', 'class': 'form-control'}),
                    **password_field_kwargs
                )
    first_name = forms.CharField(
                    widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': 'form-control'}),
                )
    last_name = forms.CharField(
                    widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': 'form-control'}),
                )
    school_code = forms.IntegerField(
                    widget=forms.NumberInput(attrs={'placeholder': 'School Code', 'class': 'form-control'}),
                )
    def clean_school_code(self):

        school_code = self.cleaned_data.get("school_code")
        if not( Institution.objects.filter(code = school_code).exists()):
            raise forms.ValidationError("please enter a valid School ID")


    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.first_name=self.cleaned_data['first_name']
        user.last_name=self.cleaned_data['last_name']
        if commit:
            user.save()
        return user
