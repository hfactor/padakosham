from django.db import models
from django.conf import settings
from institutions.models import *

# Create your models here.
User = settings.AUTH_USER_MODEL


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE)
    class Meta:
        abstract = True
class RPProfile(UserProfile):
    pass
class StudentProfile(UserProfile):
    grade = models.CharField(max_length=10,null=True,blank=True)
